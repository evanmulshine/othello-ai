package com.atomicobject.othello;

public class Move implements Comparable<Move> {
	
	private int[] pos;
	private int value;
	
	public Move(int[] position, int value) {
		pos = position;
		this.value = value;
	}
	
	/**
	 * 
	 * @return the coordinate of the move as int[]
	 */
	public int[] getPos() {
		return pos;
	}
	
	/**
	 * 
	 * @return the value of the move, as determined by AI.checkMove
	 */
	public int getValue() {
		return value;
	}

	@Override
	public int compareTo(Move move) {
		return Integer.valueOf(value).compareTo(move.getValue());
	}

}
