package com.atomicobject.othello;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class AI {
	
	// The extra weight given to moves on an edge. This is doubled for corner moves.
	private final int EDGE_WEIGHT = 2;
	private int player;
	
	public AI() {
		player = 0;
	}
	
	/**
	 * Computes the best move for the given GameState.
	 * 
	 * @param state The GameState waiting for a move.
	 * @return int array containing the coordinates of the chosen move.
	 */
	public int[] computeMove(GameState state) {
		
		// Set player
		if (player == 0) {
			player = state.getPlayer();
		}
		System.out.println("AI returning greedy move for game state - " + state);
		List<Move> moveList = getValidMoves(state);
		Move move = getGreedyMove(moveList);
		return move.getPos();
	}
	
	/**
	 * Finds all the valid moves in the given GameState.
	 * 
	 * @param state The GameState waiting for a move.
	 * @return A List of all valid moves in the given GameState.
	 */
	private List<Move> getValidMoves(GameState state) {
		ArrayList<Move> moves = new ArrayList<>();
		int[][] board = state.getBoard();
		
		// Iterate through each position on the board, checking if there is a valid move there.
		for (int i = 0; i <= 7; i++) {
			
			for (int j = 0; j <= 7; j++) {
				Move move = checkMove(board, new int[] {i,j});
				
				if (move != null) {
					moves.add(move);
				}
			}
		}
		
		return moves;
	}
	
	/**
	 * Finds the move that will flip the most discs, with extra weight given to moves on edges or corners.
	 * 
	 * @param moves The List of valid moves.
	 * @return The move that flips the most discs.
	 */
	private Move getGreedyMove(List<Move> moves) {
		Collections.sort(moves);
		Collections.reverse(moves);
		return moves.get(0);
	}
	
	/**
	 * Checks the validity of a possible move. Does not check if move is outside the bounds of the board.
	 * Also computes the weight of the move. Weight is calculated as follows: one point for each disc flipped
	 * with an extra 2 points if it is on an edge, and doubling this bonus if it is on a corner.
	 * 
	 * @param board The current board to be checked for the given move. Size is assumed to be 8x8.
	 * @param move The move being checked for validity.
	 * @return If move is valid/legal, a Move object storing the move position and the 
	 * 	number of discs it would flip. If it is invalid, returns null.
	 */
	private Move checkMove(int[][] board, int[] move) {
		Boolean valid = false;
		int row = move[0];
		int col = move[1];
		int value = 0;
		
		if (board[row][col] != 0) {
			return null;
		}
		
		if (row <= 5) {
			
			// Check straight down
			if (board[row+1][col] == (player%2)+1) { // Checks if adjacent tile has opponent's disc
				int i = row + 2;
				Boolean found = false;
				
				while (i <= 7 && !found) {
					
					if (board[i][col] == 0) { // Gap in pieces
						break;
					}
					
					if (board[i][col] == player) { // Player disc found
						found = true;
						valid = true;
						value += i - row - 1; // Find how many discs would be flipped
					}
					i++;
				}
			}
			
			// Check down-right
			if (col <= 5) {
				
				if (board[row+1][col+1] == (player%2)+1) {
					int i = row + 2;
					int j = col + 2;
					Boolean found = false;
					
					while (i <= 7 && j <= 7 && !found) {
						
						if (board[i][j] == 0) {
							break;
						}
						
						if (board[i][j] == player) {
							found = true;
							valid = true;
							value += i - row - 1;
						}
						i++;
						j++;
					}
				}
			}
			
			// Check down-left
			if (col >= 2) {
				
				if (board[row+1][col-1] == (player%2)+1) {
					int i = row + 2;
					int j = col - 2;
					Boolean found = false;
					
					while (i <= 7 && j >= 0 && !found) {
						
						if (board[i][j] == 0) {
							break;
						}
						
						if (board[i][j] == player) {
							found = true;
							valid = true;
							value += i - row - 1;
						}
						i++;
						j--;
					}
				}
			}
		}
		
		if (row >= 2) {
			
			// Check straight up
			if (board[row-1][col] == (player%2)+1) {
				int i = row - 2;
				Boolean found = false;
				
				while (i >= 0 && !found) {
					
					if (board[i][col] == 0) {
						break;
					}
					
					if (board[i][col] == player) {
						found = true;
						valid = true;
						value += row - i - 1;
					}
					i--;
				}
			}
			
			// Check up-right
			if (col <= 5) {
				
				if (board[row-1][col+1] == (player%2)+1) {
					int i = row - 2;
					int j = col + 2;
					Boolean found = false;
					
					while (i >= 0 && j <= 7 && !found) {
						
						if (board[i][j] == 0) {
							break;
						}

						if (board[i][j] == player) {
							found = true;
							valid = true;
							value += row - i - 1;
						}
						i--;
						j++;
					}
				}
			}
			
			// Check up-left
			if (col >= 2) {
				
				if (board[row-1][col-1] == (player%2)+1) {
					int i = row - 2;
					int j = col - 2;
					Boolean found = false;
					
					while (i >= 0 && j >= 0 && !found) {
						
						if (board[i][j] == 0) {
							break;
						}

						if (board[i][j] == player) {
							found = true;
							valid = true;
							value += row - i - 1;
						}
						i--;
						j--;
					}
				}
			}
		}
		
		// Check straight right
		if (col <= 5) {
			
			if (board[row][col+1] == (player%2)+1) {
				int j = col + 2;
				Boolean found = false;
				
				while (j <= 7 && !found) {
					
					if (board[row][j] == 0) {
						break;
					}
					
					if (board[row][j] == player) {
						found = true;
						valid = true;
						value += j - col - 1;
					}
					j++;
				}
			}
		}
		
		// Check straight left
		if (col >= 2) {
			
			if (board[row][col-1] == (player%2)+1) {
				int j = col - 2;
				Boolean found = false;
				
				while (j >= 0 && !found) {
					
					if (board[row][j] == 0) {
						break;
					}
					
					if (board[row][j] == player) {
						found = true;
						valid = true;
						value += col - j - 1;
					}
					j--;
				}
			}
		}
		
		
		if (valid) {
			
			// Add more weight to the value of the move if it is on an edge.
			// If it is on both edges (i.e. a corner) it has even more weight.
			if (move[0] == 0 || move[0] == 7) {
				value += EDGE_WEIGHT;
			}
			
			if (move[1] == 0 || move[1] == 7) {
				value += EDGE_WEIGHT;
			}
			return new Move(move, value);
		} else {
			return null;
		}
	}
}
